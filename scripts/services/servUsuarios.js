/**
 * Created by momentum01 on 30/6/16.
 */

app.service('servUsuarios', function($http, $q) { //$http
    
    return {
        usuarios : function () {

            var defered = $q.defer();
            var promise = defered.promise;

            $http.get('api/users.json')
                .success(function(data) {
                    defered.resolve(data);
                })
                .error(function(err) {
                    defered.reject(err)
                });

            return promise;

        }
    }
    
});