/**
 * Created by momentum01 on 1/7/16.
 */

app.service('servOrdenes', function($http, $q) { 

    return {
        ordenes : function () {

            var defered = $q.defer();
            var promise = defered.promise;

            $http.get('api/ordenes.json')
                .success(function(data) {
                    defered.resolve(data);
                })
                .error(function(err) {
                    defered.reject(err)
                });

            return promise;

        }
    }

});