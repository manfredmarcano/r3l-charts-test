/**
 * Created by momentum01 on 6/7/16.
 */

app.service('serviceData', function($http, $q) {

    return {
        datos : function () {

            var defered = $q.defer();
            var promise = defered.promise;

            $http.get('api/activity-data.json')
                .success(function(data) {
                    defered.resolve(data);
                })
                .error(function(err) {
                    defered.reject(err)
                });

            return promise;

        }
    }

});