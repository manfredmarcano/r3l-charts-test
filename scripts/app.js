/**
 * AngularJS Rokk3r Labs Test
 * @author 
 */

/**
 * Main AngularJS Web Application
 */
var app = angular.module('prueba', ['ui.router', 'chart.js']);

/**
 * Configure the Routes
 */

app.config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/analytics'); //list

    $stateProvider
        .state('news', {
            url: '/news',
            templateUrl: 'views/news.html',
            //controller: "controllerOrdenes"
        })
        

        .state('analytics', {
            url: '/analytics',
            templateUrl: 'views/analytics.html',
            controller: "lineControl"

        })
    
});


app.config(['ChartJsProvider', function (ChartJsProvider) {
    // Configure all charts
    ChartJsProvider.setOptions({
        colours: ['#FF5252', '#FF8A80'],
        responsive: true
    });
    // Configure all line charts
    ChartJsProvider.setOptions('Line', {
        datasetFill: false
    });
}]);


